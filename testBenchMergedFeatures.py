import numpy #btet3mel ma3 arkam
import cv2
from preprocessing import sobel_filter,thresholding_img,get_connected_component,show_connected_component
from edgeDirection import edgeDirection
from models import KNN , GMM,SVM
import imageModel as Img
import dataModule
import featureDetection as ftr
from sklearn import preprocessing
from sklearn.mixture import GaussianMixture
from scipy import spatial
from collections import Counter
from fractalfeature import fractalfeature,fractalfeature_line
import lowerUpperContour as ULC
import time
#leh index=0 ?
#

def testWithWriterRepresentedPerDoocument ():
    count = 0
    with open("result.txt", "a") as myfile:
        myfile.write("begin \n")
    with open("error.txt", "a") as myfile2:
        myfile2.write("begin \n")
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy=0
    ftrdetector = ftr.FeatureDetection()
    for i in range(500):
        test = dataModule.data()

        mainList = test.generateTrainingAndTestList()
        while (len(mainList)==0):
            mainList = test.generateTrainingAndTestList()
        features=[]

        #prepare the test
        featuresOfTestedImage=edge_direction_object.get_histogram_values_half_lines(mainList[2][0])
        featurevector = [[], [], [], [], [], []]
        for line in mainList[2][0].lines:
            ftrdetector.calculate_features(line)
            # ftrdetector.get_top_bottom_lines(line, True)

            featurevector[0].append(ftrdetector.f1)
            featurevector[1].append(ftrdetector.f2)
            featurevector[2].append(ftrdetector.f3)
            featurevector[3].append(ftrdetector.f4)
            featurevector[4].append(ftrdetector.f5)
            featurevector[5].append(ftrdetector.f6)
        # TODO : bfakar a5ali di most commom msh average
        featurevectortestimage = [sum(f) / len(f) for f in featurevector]
        print(featurevectortestimage)
        featuresOfTestedImage = numpy.asarray(featuresOfTestedImage.tolist() + featurevectortestimage)

        for oneImage in mainList[0] :
            features.append(edge_direction_object.get_histogram_values_half_lines(oneImage))
        for paper in mainList[0]:  # lists[0] is the trainingList 1 1 2 2 3 3
            # paper.show(0.4, imageModel.ImageModel.LINES)
            featurevector = [[], [], [], [], [], []]
            for line in paper.lines:
                ftrdetector.calculate_features(line)
                # ftrdetector.get_top_bottom_lines(line, True)

                featurevector[0].append(ftrdetector.f1)
                featurevector[1].append(ftrdetector.f2)
                featurevector[2].append(ftrdetector.f3)
                featurevector[3].append(ftrdetector.f4)
                featurevector[4].append(ftrdetector.f5)
                featurevector[5].append(ftrdetector.f6)
            # TODO : bfakar a5ali di most commom msh average
            featurevector = [sum(f) / len(f) for f in featurevector]
            print(featurevector)
            #features.append(featurevector)
            features[mainList[0].index(paper)] = numpy.append(features[mainList[0].index(paper)], featurevector)

         #now features have all images features
        allFeatures=features
        allFeatures.append(featuresOfTestedImage)
        #normalized_features = preprocessing.normalize(allFeatures)
        normalized_features=preprocessing.scale(allFeatures)
        featuresOfTestedImage =[]
        featuresOfTestedImage =normalized_features[6]
        featuresArray=normalized_features[:-1]


        resultKNN=KNN(featuresArray, mainList[1], featuresOfTestedImage)

        #result=GMM (featuresArray,1,featuresOfTestedImage)

        resultSVM=SVM(featuresArray,mainList[1],featuresOfTestedImage)

        tree = spatial.KDTree(featuresArray)
        ans = tree.query(featuresOfTestedImage)
        resultKD=mainList[1][ans[1]] #label of the nearest image to the tested image
        if(resultSVM == resultKNN and resultSVM == resultKD): # all are equal
            result=resultSVM
        elif(resultSVM==resultKNN or resultSVM==resultKD): # 2 are equal
            result=resultSVM
        elif(resultKNN==resultKD): # 2 are equal
            result=resultKNN
        else :
            result=resultKD #nothing is equal

        if(result==mainList[3][0]):
            accuracy=accuracy+1
            print("succeeded label equal", result, mainList[3][0])
            with open("result.txt", "a") as myfile:
                myfile.write("True \n")
        else :
            with open("result.txt", "a") as myfile:
                myfile.write("False \n")
            with open("error.txt", "a") as myfile2:
                myfile2.write("images paths")
                for counter in range(3):
                   for writerPath in mainList[4][counter]:
                        myfile2.write(writerPath)
                        myfile2.write(" ")
                   myfile2.write("\n")
            print ("failed wrong label is", result)

        count = count + 1
        print(count)
def testWithWriterRepresentedPerLineSlantAndHeights ():
    # testing that each writer is represneted by it's number of lines
    #features here are brenda'S and bassem's

    count = 0
    with open("result.txt", "a") as myfile:
        myfile.write("begin \n")
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy = 0

    for iteration in range(100):
        test = dataModule.data()
        ftrdetector = ftr.FeatureDetection()

        mainList = test.generateTrainingAndTestList()
        while (len(mainList) == 0):
            mainList = test.generateTrainingAndTestList()

        features = []
        featuresOfTestedImage = []
        labelList = []


        # prepare the test image
        allHistogram = edge_direction_object.get_histogram_values_lines(mainList[2][0])
        allUpperLower = ftrdetector.calculate_features_all_image(mainList[2][0])
        for i in range(len(allHistogram)):
            featuresOfTestedImage.append(numpy.append(allHistogram[i], allUpperLower[i]))

        counter = 0
        writerCounter = 1
        for oneImage in mainList[0]:
            allHistogram = edge_direction_object.get_histogram_values_lines(oneImage)
            allUpperLower = ftrdetector.calculate_features_all_image(oneImage)
            for i in range(len(allHistogram)):
                features.append(numpy.append(allHistogram[i], allUpperLower[i]))
                labelList.append(writerCounter)
            counter = counter + 1
            if (counter == 2):  # increment label counter each 2 image
                writerCounter = writerCounter + 1
                counter = 0


        allFeatures = [] #allfeatures will contain all the 7 images in order to standarize the values
        for i in range(len(features)):
            allFeatures.append(features[i])
        for i in range(len(featuresOfTestedImage)):
            allFeatures.append(featuresOfTestedImage[i])
        normalized_features = preprocessing.scale(allFeatures)

        #will slice the test image from allFeatures list
        linesOfTestedImageCount = len(featuresOfTestedImage)
        linesOfTrainingImagesCount = len(features)
        featuresArray = normalized_features[:linesOfTrainingImagesCount] #featuresArray contains lines of all 6 images
        featuresOfTestedImage = []
        featuresOfTestedImage = normalized_features[linesOfTrainingImagesCount:] #featuresOfTestedImage contains lines of the test image


        resultKNN = KNN(featuresArray, labelList, featuresOfTestedImage, True)
        resultKNN = Counter(resultKNN).most_common(1)[0][0]
        resultSVM = SVM(featuresArray, labelList, featuresOfTestedImage, True)
        resultSVM = Counter(resultSVM).most_common(1)[0][0]
        tree = spatial.KDTree(featuresArray)
        ans = tree.query(featuresOfTestedImage)
        resultKD = ans[1]  # label of the nearest image to the tested image
        KDAnswers = []
        for i in range(len(resultKD)):
            KDAnswers.append(labelList[ans[1][i]])
        resultKD = Counter(KDAnswers).most_common(1)[0][0]
        if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
            result = resultSVM
        elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
            result = resultSVM
        elif (resultKNN == resultKD):  # 2 are equal
            result = resultKNN
        else:
            result = resultKD  # nothing is equal
        if (result == mainList[3][0]):
            accuracy = accuracy + 1
            with open("result.txt", "a") as myfile:
                myfile.write("True\n")
            print("succeeded label equal", mainList[3][0])

        else:
            with open("result.txt", "a") as myfile:
                myfile.write("False \n")
            print("failed wrong label is", result, "shouldbe", mainList[3][0])
        count = count + 1

#testWithWriterRepresentedPerLineSlantAndHeights()
def testWithWriterRepresentedPerLinesSlantAndFractional ():
    # testing that each writer is represneted by it's number of lines

    count = 0
    with open("result.txt", "a") as myfile:
        myfile.write("begin \n")
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy = 0
    accuracyFractal=0
    num_of_iterations=500


    for iteration in range(100):

        #prepare Data
        test = dataModule.data()
        mainList = test.generateTrainingAndTestList()
        while (len(mainList) == 0):
            mainList = test.generateTrainingAndTestList()



        features = []
        fractalfeatures=[]
        fractalfeature_test=[]
        featuresOfTestedImage = []
        labelList = []
        writerCounter = 1
        counter = 0
        numberOfLinesUsed=-1
        # prepare the test image
        allHistogram = edge_direction_object.get_histogram_values_lines(mainList[2][0], numberOfLinesUsed)
        fractalFeature=fractalfeature(mainList[2][0],numberOfLinesUsed)
        for i in range(len(allHistogram)):
            #featuresOfTestedImage.append(allHistogram[i])
            featuresOfTestedImage.append(allHistogram[i])
            fractalfeature_test.append(fractalFeature[i])
        #TODO : Bisho , add your features on each array in featuresOfTestedImage , featuresOfTestedImage is a list of numpy array



        for oneImage in mainList[0]:
            allHistogram = edge_direction_object.get_histogram_values_lines(oneImage, numberOfLinesUsed)
            fractalFeature = fractalfeature(oneImage, numberOfLinesUsed)
            for i in range(len(allHistogram)):
                #features.append(allHistogram[i])
                features.append(allHistogram[i])
                fractalfeatures.append(fractalFeature[i])
                labelList.append(writerCounter)
            counter = counter + 1
            if (counter == 2):  # increment label counter each 2 image
                writerCounter = writerCounter + 1
                counter = 0
        # TODO : Bisho , add your features on each array in features, features is a list of numpy arr



        #merging all images in order to standardize the values
        allFeatures = features.copy()
        allFeatures_fractal=fractalfeatures.copy()
        for i in range(len(featuresOfTestedImage)):
            allFeatures.append(featuresOfTestedImage[i])
            allFeatures_fractal.append(fractalfeature_test[i])
        normalized_features = preprocessing.scale(allFeatures)
        normalized_features_fractal=preprocessing.scale(allFeatures_fractal)

        #slice the test image from the lists
        linesOfTestedImageCount = len(featuresOfTestedImage)
        linesOfTrainingImagesCount = len(features)
        featuresArray = normalized_features[:linesOfTrainingImagesCount]
        featuresArrayFractal=normalized_features_fractal[:linesOfTrainingImagesCount]
        featuresOfTestedImage = []
        featuresOfTestedImageFractal = []
        featuresOfTestedImage = normalized_features[linesOfTrainingImagesCount:]
        featuresOfTestedImageFractal=normalized_features_fractal[linesOfTrainingImagesCount:]




        print("CORRECT ONE IS ",mainList[3][0])
        #time to give the values for models
        print ("KNN values for slant")
        resultKNN = KNN(featuresArray, labelList, featuresOfTestedImage,True)
        print(resultKNN)
        resultKNN=Counter(resultKNN).most_common(1)[0][0]
        print("choosen",resultKNN)

        print("KNN values for fractal")
        resultKNNFractal = KNN(featuresArrayFractal, labelList, featuresOfTestedImageFractal, True)
        print(resultKNNFractal)
        resultKNNFractal=Counter(resultKNNFractal).most_common(1)[0][0]
        print("choosen",resultKNNFractal)

        print(" SVM for slant ")
        resultSVM = SVM(featuresArray, labelList, featuresOfTestedImage, True)
        print(resultSVM)
        resultSVM = Counter(resultSVM).most_common(1)[0][0]
        print("choosen",resultSVM)

        print(" SVM for fractal ")
        resultSVMFractal = SVM(featuresArrayFractal, labelList, featuresOfTestedImageFractal, True)
        print(resultSVMFractal)
        resultSVMFractal = Counter(resultSVMFractal).most_common(1)[0][0]
        print("choosen", resultSVMFractal)


        print(" KDTree for slant")
        tree = spatial.KDTree(featuresArray)
        ans = tree.query(featuresOfTestedImage)
        resultKD =ans[1]  # label of the nearest image to the tested image
        KDAnswers=[]
        for i in range (len(resultKD)):
            KDAnswers.append(labelList[ans[1][i]])
        print(KDAnswers)
        resultKD=Counter(KDAnswers).most_common(1)[0][0]
        print("choosen", resultKD)


        print("KD for fractal")
        tree = spatial.KDTree(featuresArrayFractal)
        ans = tree.query(featuresOfTestedImageFractal)
        resultKDFractal = ans[1]  # label of the nearest image to the tested image
        KDAnswersFractal = []
        for i in range(len(resultKDFractal)):
            KDAnswersFractal.append(labelList[ans[1][i]])
        print(KDAnswersFractal)
        resultKDFractal = Counter(KDAnswersFractal).most_common(1)[0][0]
        print("choosen", resultKDFractal)




        #voting time
        if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
            result = resultSVM
        elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
            result = resultSVM
        elif (resultKNN == resultKD):  # 2 are equal
            result = resultKNN
        else:
            result = resultSVM  # nothing is equal

        print("slant choose ",result)

        #decision time
        # result = resultSVM
        if (result == mainList[3][0]):
            accuracy = accuracy + 1


        # voting time
        if (resultSVMFractal == resultKNNFractal and resultSVMFractal == resultKDFractal):  # all are equal
            result = resultSVMFractal
        elif (resultSVMFractal == resultKNNFractal or resultSVMFractal == resultKDFractal):  # 2 are equal
            result = resultSVMFractal
        elif (resultKNNFractal == resultKDFractal):  # 2 are equal
            result = resultKNNFractal
        else:
            result = resultKNNFractal  # nothing is equal

            print("fractal choose ", result)

            if (result == mainList[3][0]):
                accuracyFractal = accuracyFractal + 1

        print("the accuracy= ", accuracy / num_of_iterations)
        print("the accuracy for fractal", accuracyFractal / num_of_iterations)
        count = count + 1


def testWithWriterRepresentedPerLinesSlantAndFractionalAndLowerUpper ():

    for iteration in range(100) :
        startImport = time.time()

        brendaCalTime = 0
        meAndBishoCalTime = 0

        startBrendaTime = time.time()
        edge_direction_object = edgeDirection()
        endBrendaTime = time.time()
        brendaCalTime += (endBrendaTime - startBrendaTime)

        print("############################iteration number ", iteration)

        test = dataModule.data()
        mainlist = test.generateTrainingAndTestList()
        while (len(mainlist) == 0):
            mainlist = test.generateTrainingAndTestList()
        training_features = []
        training_labels = []

        startCalculations = time.time()

        for idx, paper in enumerate(mainlist[0]):

            startBrendaTime = time.time()
            allHistogram = edge_direction_object.get_histogram_values_lines(paper, len(paper.lines))
            endBrendaTime = time.time()

            brendaCalTime += (endBrendaTime - startBrendaTime)

            for lidx, line in enumerate(paper.lines):
                startMeAndBisho = time.time()

                # list features of training example
                # each one is 1-D array
                uc = ULC.UpperLowerContour(line, True)
                # ul = ULC.UpperLowerContour(line, False)
                slopes = fractalfeature_line(line)

                # concatenate all features lists to 1-D list
                training_features.append(uc.getFeatureVector() + slopes + allHistogram[lidx].tolist())
                training_labels.append(mainlist[1][idx])

                endMeAndBishoTime = time.time()
                meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

        test_features = []

        startBrendaTime = time.time()

        allHistogramTest = edge_direction_object.get_histogram_values_lines(mainlist[2][0], len(mainlist[2][0].lines))

        endBrendaTime = time.time()
        brendaCalTime += (endBrendaTime - startBrendaTime)

        for lidx, line in enumerate(mainlist[2][0].lines):
            startMeAndBisho = time.time()
            # list features of testing example
            # each one is 1-D array
            uct = ULC.UpperLowerContour(line, True)
            # ult = ULC.UpperLowerContour(line, False)
            slopest = fractalfeature_line(line)

            # concatenate all features lists to 1-D list
            test_features.append(uct.getFeatureVector() + slopest + allHistogramTest[lidx].tolist())

            endMeAndBishoTime = time.time()
            meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

        # combine and scale all features
        allFeatures = numpy.array(training_features + test_features)
        normalized_features = preprocessing.scale(allFeatures)

        slantOnly = list(map(list, zip(*normalized_features)))[9:]
        slantOnly= list(map(list, zip(*slantOnly)))
        slantOnly=numpy.array(slantOnly)

        # split training set
        withoutLastOne = normalized_features[:-len(test_features), :]
        withoutLastOneSlant= slantOnly[:-len(test_features), :]

        lastOne = normalized_features[-len(test_features):, :]
        lastOneSlant=slantOnly[-len(test_features):, :]

        endCalculations = time.time()

        # KNN CLASSIFIER


        resultKNN = KNN(withoutLastOne, training_labels, lastOne, True)
        resultKNN = Counter(resultKNN).most_common(1)[0][0]
        print("Result KNN:    ",resultKNN )



        tree = spatial.KDTree(withoutLastOne)
        ans = tree.query(lastOne)
        resultKD = ans[1]  # label of the nearest image to the tested image
        KDAnswers = []
        for i in range(len(resultKD)):
            KDAnswers.append(training_labels[ans[1][i]])
        resultKD = Counter(KDAnswers).most_common(1)[0][0]
        print("Result KDTREE: ", resultKD)



        resultSVM = SVM(withoutLastOneSlant, training_labels, lastOneSlant, True)
        resultSVM = Counter(resultSVM).most_common(1)[0][0]
        print("Result SVM:    ", resultSVM)

        print("CORRECT: ", mainlist[3][0])
        if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
            result = resultSVM
        elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
            result = resultSVM
        elif (resultKNN == resultKD):  # 2 are equal
            result = resultKNN
        else:
            result = resultSVM  # nothing is equal


        print("Calculation Time: (", endCalculations - startCalculations, "s)", "[Brenda only]:", brendaCalTime, " ",
              "[Me&bisho]:", meAndBishoCalTime, " ",
              (endCalculations - startCalculations) * 100 / (endCalculations - startImport), "%  ,Img Time:",
              (startCalculations - startImport) * 100 / (endCalculations - startImport), "% ,Total:",
              endCalculations - startImport, "s")

        # return 1 if correct,else 0
        if result == mainlist[3][0]:
            print("SUCCESS")
testWithWriterRepresentedPerLinesSlantAndFractionalAndLowerUpper ()



