import imageModel as Img
import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from scipy.stats import linregress
from scipy.spatial import distance

class UpperLowerContour:


    CONN_TOLERANCE = 1

    def __init__(self,line,isUpper=True):

        self.image = line
        self.upper= isUpper

        self.contour_X=[]
        self.contour_Y=[]

    def getULContour(self):

        #get binary image
        _, binaryImg = cv2.threshold(self.image, 150, 255, cv2.THRESH_BINARY)

        #make some erosion to guarantee continuity of letters pixel
        kernel = np.ones((4, 4), np.uint8)
        binaryImg = cv2.erode(binaryImg, kernel, iterations=1)

        # transpose for change cols with rows
        transposedImg = cv2.transpose(binaryImg)

        imgH, imgW = transposedImg.shape[:2]

        self.imgHeight = imgH

        conX= []
        conY = []

        needShift=[]

        scannedRow = 0
        actualRow=0
        baseY= None
        connectivityTolerance=0

        while(scannedRow < imgH-1):

            #loop untill found non empty col
            while(np.isclose(transposedImg[scannedRow], transposedImg[scannedRow][0]).all() and scannedRow < imgH-1):
                scannedRow+=1
                connectivityTolerance-=1

            #check if there is a pixel with 0 value -> ink
            if not(np.isclose(transposedImg[scannedRow], transposedImg[scannedRow][0]).all()):

                if self.upper:
                    reversedRow = transposedImg[scannedRow][::-1]
                else:
                    reversedRow = transposedImg[scannedRow]

                pixelIndex = np.where(reversedRow == 0)[0]
                colIdx = np.max(pixelIndex)


                if baseY is None:
                    baseY = colIdx
                    needShift.append(False)
                else:
                    if connectivityTolerance < 0:
                        needShift.append(True)
                    else:
                        needShift.append(False)

                baseY = colIdx

                conX.append(actualRow)
                if self.upper:
                    conY.append(baseY)
                else:
                    conY.append(imgH-baseY)

                scannedRow += 1
                actualRow+=1

                connectivityTolerance= UpperLowerContour.CONN_TOLERANCE

        # shift y values to eliminate discontinuities in y-axis
        for outIdx in range(len(conX)):
            if needShift[outIdx]:
                diff = conY[outIdx] - conY[outIdx - 1]
                for inIdx in range(outIdx,len(conX)):
                    conY[inIdx]+=abs(diff)


        self.contour_Y = conY
        self.contour_X = conX


    def getLocalMaxima(self):
        peaks, _ = find_peaks(self.contour_Y)
        return peaks

    def getLocalMinima(self):
        modConY = np.array(self.contour_Y)
        modConY = self.imgHeight - modConY
        return find_peaks(modConY)[0]


    def getLineSlopeAndMSE(self):
        slope, intercept, _, _, std_err = linregress(self.contour_X,self.contour_Y)
        coX = np.array(self.contour_X)
        mse = sum((self.contour_Y - (slope * coX.astype(float)+ intercept)) ** 2) / (len(self.contour_X) - 2)
        return [slope,mse]

    def calMaxMinFrequencies(self):
        return [len(self.getLocalMaxima())/len(self.contour_X) , len(self.getLocalMinima())/len(self.contour_X)]

    #calculate avg distance between every two consecutive local minima and maxima
    def calAvgDistanceBetweenLocalMaximaMinima(self):

        localMaximas = self.getLocalMaxima()
        localMinimas = self.getLocalMinima()

        avgMaxDistance=0
        avgMinDistance=0

        for idx in range(1,len(localMaximas)):
            avgMaxDistance+=distance.euclidean((localMaximas[idx-1],self.contour_Y[localMaximas[idx-1]]),(localMaximas[idx],self.contour_Y[localMaximas[idx]]))

        for idx in range(1,len(localMinimas)):
            avgMinDistance+=distance.euclidean((localMinimas[idx-1],self.contour_Y[localMinimas[idx-1]]),(localMinimas[idx],self.contour_Y[localMinimas[idx]]))

        return [avgMaxDistance/len(localMaximas),avgMinDistance/len(localMinimas)]


    # return 6 values (slope,MSE,freqOfMaxima,freqOfMinima,avgDistaceMaximas,avgDistanceMinimas)
    def getFeatureVector(self):
        self.getULContour()
        return self.getLineSlopeAndMSE()+self.calMaxMinFrequencies()+self.calAvgDistanceBetweenLocalMaximaMinima()

    def plotContour(self):
        plt.plot(self.contour_X,self.contour_Y)
        plt.show()

if __name__ == '__main__':
    img = Img.ImageModel('dataset/r03-026.png')
    ulcF = UpperLowerContour(img.lines[0],False)
    print(ulcF.getFeatureVector())
    ulcF.plotContour()
