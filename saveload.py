import imageModel


def save1d(file, list):
    with open(file, 'w') as f:
        for item in list:
            if isinstance(item, imageModel.ImageModel):
                f.write("%s\n" % item.path)
            else:
                f.write("%s\n" % item)


def save2d(file, list):
    with open(file, 'w') as f:
        for item in list:
            for i in item:
                f.write("%s " % i)
            f.write("\n")


def load1d(file, integers=False):
    list = []
    with open(file) as f:
        lines = f.read().splitlines()
        for line in lines:
            number_strings = line.split()
            if integers:
                numbers = [float(n) for n in number_strings]  # Convert to integers
                list.append(numbers[0])  # Add the "row" to your list.
            else:
                list.append(number_strings[0])  # Add the "row" to your list.
    return list


def load2d(file):
    list = []
    with open(file) as f:
        lines = f.read().splitlines()
        for line in lines:
            number_strings = line.split()
            numbers = [float(n) for n in number_strings]  # Convert to integers
            list.append(numbers)  # Add the "row" to your list.
    return list


def loadimagelist(list):
    returned_list = []
    for path in list:
        img = imageModel.ImageModel(path)
        returned_list.append(img)
    return returned_list

