import dataModule
from fractalfeature import fractalfeature
from sklearn.neighbors import KNeighborsClassifier
from collections import Counter
from statistics import mode
import statistics
import numpy
import time


if __name__ == '__main__':
    dumm = dataModule.data()
    correct = 0
    num_of_iterarions = 100
    start = time.time()
    for i in range(num_of_iterarions):
        print("############################iteration number ", i + 1)
        # lists[0]->imagemodels of training
        # lists[1]->labels of training
        # lists[2]->imagemodel of test
        # lists[3]->label of test
        mainlist = dumm.generateTrainingAndTestList()
        while (len(mainlist) == 0):
            mainlist = dumm.generateTrainingAndTestList()
        features = []
        featuresOfTestedImage = []
        labelList = []
        numberOfLinesUsed = -1
        #num_of_dilations=30
        for idx, paper in enumerate(mainlist[0]):
            fractalFeature = fractalfeature(paper, numberOfLinesUsed)
            for i in range(len(fractalFeature)):
                features.append(fractalFeature[i])
                labelList.append(mainlist[1][idx])
        fractalFeature_test = fractalfeature(mainlist[2][0], numberOfLinesUsed)
        for k in range(len(fractalFeature_test)):
            # featuresOfTestedImage.append(allHistogram[i])
            featuresOfTestedImage.append(fractalFeature_test[k])

        knn = KNeighborsClassifier(n_neighbors=5)
        knn.fit(features, labelList)
        lines_ans = knn.predict(featuresOfTestedImage)
        print(lines_ans)
        ans = Counter(lines_ans).most_common(1)[0][0]
        if ans == mainlist[3][0]:
            correct = correct + 1
            print("succeed the answer is ", mainlist[3][0])
        else:
            print("failed the correct answer is ", mainlist[3][0])
        print("num of correct=", correct)
    end = time.time()
    print("total time = ",end - start)
    accuracy = correct / num_of_iterarions
    print(accuracy * 100)
