import random
import fileDirectory
import imageModel as Img
from sklearn.decomposition import PCA
import scipy
import matplotlib.pyplot as plt
class data :
    def generate3RandomWriters(self,file):
        #input : 2D list named file , you can give it any id of user and it will return a list with all his image paths
        # functionality :generate paths of 3 writers , 2 document per each , i discard all writers that have 1 or 2 document only
        # output: return list of 3 lists , each list contains the paths of each user
        writerPaths=[]
        self.generate1TestWriter()
        for i in range(3):
            id=random.randint(1,671)
            writerPath = file.load_user_images(id)
            writer = []
            while True:
                if ((not writerPath )or len(writerPath)<3):
                    id = random.randint(0, 671)
                    # choose a random writer id
                    writerPath = file.load_user_images(id)
                    # get the writer paths
                else :
                    if (i == self.writerWillBeTested):
                        #take 3 images because we need one for testing
                        writer.append(writerPath[0])
                        writer.append(writerPath[1])
                        writer.append(writerPath[2])

                    else :
                        # take 2 document per writer
                        writer.append(writerPath[0])
                        writer.append(writerPath[1])
                    break
            writerPaths.append(writer)
        return writerPaths
    def generate1TestWriter(self):
        # function :generate a random number between 0 and 2 , to use it to pick the writer whose writing will be the test
        self.writerWillBeTested=random.randint(0, 2)
    def generateTrainingAndTestList(self):
        # for each image path for each writer construct an image object from image model
        # output : List that contains 4 lists
        # the first list is a list that contains 6 images object (the training list)
        # the second list is the labels of the training list
        # the third list is the image object to be tested
        # the 4th list is the label of the test
        # the 5th list is the list of the image paths
        file=fileDirectory.File()
        file.load_file()
        writersPath=self.generate3RandomWriters(file) #generate 3 writers
        #writersPath=[['dataset/m04-145.png', 'dataset/m04-152.png'], ['dataset/a01-053.png', 'dataset/a01-058.png', 'dataset/a01-063.png'], ['dataset/a04-081.png', 'dataset/a04-085.png']]
        #self.writerWillBeTested=1
        #now we have all paths of images required
        print(writersPath)
        trainingList=[] # will contain list of object of image model and not the image itself
        labelList=[]
        test=[]
        labelTest=[]
        generalList=[]
        firstTime=True
        #this for loop construct the image model object for each path
        for counter in range(3) :
            for pathOfAImg in writersPath[counter] :

                if (self.writerWillBeTested==counter):
                #if it's the writer that will have an extra image for test , load this extra image and put it in the test []
                    if(firstTime):
                        print("now constructing",pathOfAImg)
                        imgObject=Img.ImageModel(pathOfAImg)
                        if (imgObject.found==True):
                            # . found check if the image is correctly loaded
                            test.append(imgObject)
                            labelTest.append(counter + 1)
                            firstTime=False #to not include the test in training
                            if(len(writersPath[counter])==1): # i think this is a dead code but i'm not sure i will revisit it again
                                trainingList.append(imgObject)
                                labelList.append(counter + 1)
                        else :
                            #if an error happened return empty list
                            return []

                    else:
                        print("now constructing", pathOfAImg)
                        imgObject = Img.ImageModel(pathOfAImg)
                        if (imgObject.found == True):
                            # . found check if the image is correctly loaded
                         trainingList.append(imgObject)
                         labelList.append(counter+1)
                        else :
                            return []
                else :
                    print("now constructing", pathOfAImg)
                    imgObject = Img.ImageModel(pathOfAImg)
                    if (imgObject.found == True):
                        # . found check if the image is correctly loaded
                     trainingList.append(imgObject)
                     labelList.append(counter+1)
                    else :
                        return []

        generalList.append(trainingList)
        generalList.append(labelList)
        generalList.append(test)
        generalList.append(labelTest)
        generalList.append(writersPath)
        return generalList
    def plotData(self,features):
        pca = PCA(n_components=2)
        principalComponents = pca.fit_transform(features)
        componentX=[]
        componentY=[]
        for i in range(len(principalComponents)):
            componentX.append(principalComponents[i][0])
            componentY.append(principalComponents[i][1])




