import imageModel as Img
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
from scipy import stats


def create_circular_mask(radius):
    kernel = np.zeros((2 * radius + 1, 2 * radius + 1),np.uint8)
    y, x = np.ogrid[-radius:radius + 1, -radius:radius + 1]
    mask = x ** 2 + y ** 2 <= radius ** 2
    kernel[mask] = 1
    return kernel


def get_fitted_line(x1,y1,x2,y2):
    xtofit = [[x1], [x2]]
    ytofit = [y1, y2]
    regr = linear_model.LinearRegression()
    regr.fit(xtofit, ytofit)
    return regr

def draw(x_axis,y_axis,points):
    slopes=[]
    reg = get_fitted_line(x_axis[0][0], y_axis[0], x_axis[points[0]][0], y_axis[points[0]])
    slopes.append(reg.coef_[0])
    y_predicted1 = reg.predict(x_axis[0:points[0] + 1])
    reg = get_fitted_line(x_axis[points[0]][0], y_axis[points[0]], x_axis[points[1]][0], y_axis[points[1]])
    slopes.append(reg.coef_[0])
    y_predicted2 = reg.predict(x_axis[points[0]:points[1] + 1])
    reg = get_fitted_line(x_axis[points[1]][0], y_axis[points[1]], x_axis[-1][0], y_axis[-1])
    slopes.append(reg.coef_[0])
    y_predicted3 = reg.predict(x_axis[points[1]:])
    # plt.scatter(x_axis, y_axis, marker='x', color='black')
    # plt.plot(x_axis[0:points[0] + 1], y_predicted1)
    # plt.plot(x_axis[points[0]:points[1] + 1], y_predicted2)
    # plt.plot(x_axis[points[1]:], y_predicted3)
    # plt.show()
    return slopes
def get_slope(x1,y1,x2,y2):
    m=(y2-y1)/(x2-x1)
    c=y1-(m*x1)
    return m,c

def fractalfeature_line(inputline,num_of_dilations=19):
    # dilation = cv2.erode(binary, kernel, iterations=5)
    _,line=cv2.threshold(inputline, 150, 255, cv2.THRESH_BINARY_INV)
    x_axis=[]
    y_axis=[]
    for i in range(1,19):
        kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(i,i))
        dilated=cv2.dilate(line,kernel,iterations=1)
        x_axis.append(np.log(i))
        y_axis.append(np.log(cv2.countNonZero(dilated))-np.log(i))

    # plt.scatter(x_axis,y_axis,marker='x',color='black')
    # plt.show()
    slopes=[]
    slopes.append(get_slope(x_axis[0],y_axis[0],x_axis[1],y_axis[1])[0])
    x_axis=x_axis[2:]
    y_axis=y_axis[2:]
    points=[]
    for k in range(0,2):
        # print("line number ",k)
        indx=[]
        err=[]
        # if k==0:
        #     n=1
        # else:
        #     n=1
        for i in range(1,len(x_axis)):
            # reg=get_fitted_line(x_axis[min_error_point][0],y_axis[min_error_point],x_axis[i][0],y_axis[i])
            # y_predicted=reg.predict(x_axis[min_error_point:i+1])
            m = (y_axis[i] - y_axis[0]) / (x_axis[i]- x_axis[0])
            c = y_axis[0] - (m * x_axis[0])
            y_predicted=[]
            for j in range(1,len(x_axis)):
                y_predicted.append(m*x_axis[j]+c)
            error=mean_squared_error(y_axis[1:], y_predicted)
            #print("mean square error",error )
            indx.append(i)
            err.append(error)
        err_sorted=np.sort(err)
        #print("min error=",err.index(err_sorted[0])+1)
        # plt.plot(indx,err)
        # plt.show()
        min_error_point=err.index(err_sorted[0])+1
        c=0
        while k==0 and min_error_point>len(x_axis)-3:
            c=c+1
            min_error_point=err.index(err_sorted[c])+1
        slopes.append(get_slope(x_axis[0],y_axis[0],x_axis[min_error_point],y_axis[min_error_point])[0])
        x_axis = x_axis[min_error_point+1:]
        y_axis = y_axis[min_error_point+1:]


    #print(slopes)
    #slopes=draw(x_axis, y_axis, points)
    return slopes
    # print(slopes)

def fractalfeature(imagemodel,num_of_lines=-1,num_of_dilations=19):
    linescount=len(imagemodel.lines)
    if num_of_lines==-1:
        num_of_lines=linescount
    elif(linescount<num_of_lines):
        num_of_lines=linescount
    print("num of lines",num_of_lines)
    features=[]
    for i in range(num_of_lines):
        slopes=fractalfeature_line(imagemodel.lines[i],num_of_dilations)
        features.append(slopes)
    return features










