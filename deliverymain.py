
import os
import sys
import imageModel as img
from edgeDirection import edgeDirection
from sklearn import preprocessing
from models import KNN,SVM
from collections import Counter
from scipy import spatial
from fractalfeature import fractalfeature,fractalfeature_line
import lowerUpperContour as ULC
import time
import numpy
# IMPORTANT
# use this flag to differentiate the pre processing between the papers wit the top printed area and clear paper
PRINTED_AREA = False

walk_dir = "datakolia"
print('walk_dir = ' + walk_dir)
#TODO : change list of dirs to int , sort it then n7welha tani
#TODO change jpg l format el tslim
#TODO hena el test dimen writer 1 change it

def test1 ():
    start = time.time()
    dirs = [d for d in os.listdir('datakolia') if os.path.isdir(os.path.join('datakolia', d))]
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy = 0
    count=0
    numberOfLinesUsed = -1
    dirs=map(int,dirs)
    dirs=sorted(dirs)
    print(dirs)
    for testfoldername in dirs:
        testfolderpath = os.path.join(walk_dir, testfoldername)
        print(testfolderpath)
        testImageObject = img.ImageModel(os.path.join(testfolderpath, "test.jpg"), PRINTED_AREA)
        ImagesObjectsList = []
        Labels = [1, 1, 2, 2, 3, 3]

        for onetwothree in [1, 2, 3]:
            writerfolderpath = os.path.join(testfolderpath, onetwothree.__str__())
            print(writerfolderpath)
            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "1.jpg"), PRINTED_AREA))
            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "2.jpg"), PRINTED_AREA))


        # per test code can be done here, WITH THIS INDENTATION
        # the test image is @ 'testImageObject'
        # the dataset images are in @ 'ImagesObjectsList' with @ 'Labels' equal arranged as 1 1 2 2 3 3
        # the labels are in @ 'Labels' list

        features = []
        featuresOfTestedImage = []
        labelList = []
        writerCounter = 1
        counter = 0


        # prepare the image test

        allHistogram = edge_direction_object.get_histogram_values_lines(testImageObject, numberOfLinesUsed)
        for i in range(len(allHistogram)):
            featuresOfTestedImage.append(allHistogram[i])

        for oneImage in ImagesObjectsList:
            allHistogram = edge_direction_object.get_histogram_values_lines(oneImage, numberOfLinesUsed)
            for i in range(len(allHistogram)):
                features.append(allHistogram[i])
                labelList.append(writerCounter)
            counter = counter + 1
            if (counter == 2):  # increment label counter each 2 image
                writerCounter = writerCounter + 1
                counter = 0

        allFeatures = []
        for i in range(len(features)):
            allFeatures.append(features[i])
        for i in range(len(featuresOfTestedImage)):
            allFeatures.append(featuresOfTestedImage[i])
        normalized_features = preprocessing.scale(allFeatures)
        linesOfTestedImageCount = len(featuresOfTestedImage)
        linesOfTrainingImagesCount = len(features)
        featuresArray = normalized_features[:linesOfTrainingImagesCount]
        featuresOfTestedImage = []
        featuresOfTestedImage = normalized_features[linesOfTrainingImagesCount:]

        # time to enter values on Models

        # resultKNNList = KNN(featuresArray, labelList, featuresOfTestedImage,True)
        # resultKNN=Counter(resultKNNList).most_common(1)[0][0]
        #
        # resultSVMList = SVM(featuresArray, labelList, featuresOfTestedImage, True)
        # resultSVM = Counter(resultSVMList).most_common(1)[0][0]

        tree = spatial.KDTree(featuresArray)
        ans = tree.query(featuresOfTestedImage)
        resultKD =ans[1]  # label of the nearest image to the tested image
        KDAnswers=[]

        for i in range (len(resultKD)):
            KDAnswers.append(labelList[ans[1][i]])
            resultKD=Counter(KDAnswers).most_common(1)[0][0]
        result=resultKD
        # voting time
        # if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
        #     result = resultSVM
        # elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
        #     result = resultSVM
        # elif (resultKNN == resultKD):  # 2 are equal
        #     result = resultKNN
        # else:
        #     result = resultSVM  # nothing is equal


        # Decision time

        if (result == 1):
            accuracy = accuracy + 1
            with open("result.txt", "a") as myfile:
                myfile.write("True\n")
            print("succeeded label equal", 1)

        else:
            with open("result.txt", "a") as myfile:
                myfile.write("False \n")
            with open("error.txt", "a") as myfile2:
                myfile2.write("test case ")
                myfile2.write(writerfolderpath)
                myfile2.write("\n")
                myfile2.write(" failed wrong label is ")
                myfile2.write(str(result))
                myfile2.write("\n")
                # myfile2.write("KDTREE")
                # for i in range(len(KDAnswers)):
                #     myfile2.write(str(KDAnswers[i]))
                # myfile2.write("\n")
                # myfile2.write("SVM")
                # for i in range(len(resultSVMList)):
                #     myfile2.write(str(resultSVMList[i].item()))
                # myfile2.write("\n")
                # myfile2.write("KNN ")
                # for i in range(len(resultKNNList)):
                #     myfile2.write(str(resultKNNList[i].item()))

            print("failed wrong label is", result, "shouldbe", 1)
        count = count + 1
    end = time.time()
    print(end-start, "time")
    print("accuracy equal",accuracy,"number of iterations",count)
def test2():
    result=[]
    start = time.time()
    dirs = [d for d in os.listdir('data') if os.path.isdir(os.path.join('data', d))]
    edge_direction_object = edgeDirection()
    #window_size = 12
    accuracy = 0
    count = 0
    numberOfLinesUsed = -1
    iteration=0
    for testfoldername in dirs:
        testfolderpath = os.path.join(walk_dir, testfoldername)
        print(testfolderpath)
        testImageObject = img.ImageModel(os.path.join(testfolderpath, "test.jpg"), PRINTED_AREA)
        ImagesObjectsList = []
        Labels = [1, 1, 2, 2, 3, 3]

        for onetwothree in [1, 2, 3]:
            writerfolderpath = os.path.join(testfolderpath, onetwothree.__str__())
            print(writerfolderpath)
            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "1.jpg"), PRINTED_AREA))
            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "2.jpg"), PRINTED_AREA))
        startImport = time.time()

        brendaCalTime = 0
        meAndBishoCalTime = 0

        startBrendaTime = time.time()
        edge_direction_object = edgeDirection()
        endBrendaTime = time.time()
        brendaCalTime += (endBrendaTime - startBrendaTime)

        print("############################iteration number ", iteration)
        training_features = []
        training_labels = []

        startCalculations = time.time()

        for idx, paper in enumerate(ImagesObjectsList):

            startBrendaTime = time.time()
            allHistogram = edge_direction_object.get_histogram_values_lines(paper, len(paper.lines))
            endBrendaTime = time.time()

            brendaCalTime += (endBrendaTime - startBrendaTime)

            for lidx, line in enumerate(paper.lines):
                startMeAndBisho = time.time()

                # list features of training example
                # each one is 1-D array
                uc = ULC.UpperLowerContour(line, True)
                # ul = ULC.UpperLowerContour(line, False)
                slopes = fractalfeature_line(line)

                # concatenate all features lists to 1-D list
                training_features.append(uc.getFeatureVector() + slopes + allHistogram[lidx].tolist())
                training_labels.append(Labels[idx])

                endMeAndBishoTime = time.time()
                meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

        test_features = []

        startBrendaTime = time.time()

        allHistogramTest = edge_direction_object.get_histogram_values_lines(testImageObject , len(testImageObject .lines))

        endBrendaTime = time.time()
        brendaCalTime += (endBrendaTime - startBrendaTime)

        for lidx, line in enumerate(testImageObject.lines):
            startMeAndBisho = time.time()
            # list features of testing example
            # each one is 1-D array
            uct = ULC.UpperLowerContour(line, True)
            # ult = ULC.UpperLowerContour(line, False)
            slopest = fractalfeature_line(line)

            # concatenate all features lists to 1-D list
            test_features.append(uct.getFeatureVector() + slopest + allHistogramTest[lidx].tolist())

            endMeAndBishoTime = time.time()
            meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

        # combine and scale all features
        allFeatures = numpy.array(training_features + test_features)
        normalized_features = preprocessing.scale(allFeatures)

        slantOnly = list(map(list, zip(*normalized_features)))[9:]
        slantOnly = list(map(list, zip(*slantOnly)))
        slantOnly = numpy.array(slantOnly)

        # split training set
        withoutLastOne = normalized_features[:-len(test_features), :]
        withoutLastOneSlant = slantOnly[:-len(test_features), :]

        lastOne = normalized_features[-len(test_features):, :]
        lastOneSlant = slantOnly[-len(test_features):, :]

        endCalculations = time.time()

        # KNN CLASSIFIER


        resultKNN = KNN(withoutLastOne, training_labels, lastOne, True)
        resultKNN = Counter(resultKNN).most_common(1)[0][0]
        print("Result KNN:    ", resultKNN)

        tree = spatial.KDTree(withoutLastOne)
        ans = tree.query(lastOne)
        resultKD = ans[1]  # label of the nearest image to the tested image
        KDAnswers = []
        for i in range(len(resultKD)):
            KDAnswers.append(training_labels[ans[1][i]])
        resultKD = Counter(KDAnswers).most_common(1)[0][0]
        print("Result KDTREE: ", resultKD)

        resultSVM = SVM(withoutLastOne, training_labels, lastOne, True)
        resultSVM = Counter(resultSVM).most_common(1)[0][0]
        print("Result SVM:    ", resultSVM)

        print("CORRECT: ", 1)
        if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
            result = resultSVM
        elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
            result = resultSVM
        elif (resultKNN == resultKD):  # 2 are equal
            result = resultKNN
        else:
            result = resultSVM  # nothing is equal

        print("Calculation Time: (", endCalculations - startCalculations, "s)", "[Brenda only]:", brendaCalTime, " ",
              "[Me&bisho]:", meAndBishoCalTime, " ",
              (endCalculations - startCalculations) * 100 / (endCalculations - startImport), "%  ,Img Time:",
              (startCalculations - startImport) * 100 / (endCalculations - startImport), "% ,Total:",
              endCalculations - startImport, "s")

        # return 1 if correct,else 0
        if result == 1:
            print("SUCCESS")
        iteration=iteration+1


if __name__ == "__main__":

    #test1()
    test2()