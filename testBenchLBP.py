import dataModule
from sklearn.neighbors import KNeighborsClassifier
from collections import Counter
from LBP import LocalBinaryPatterns
from statistics import mode
import statistics
import numpy
import cv2
from models import SVM
from sklearn.svm import LinearSVC
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dumm = dataModule.data()
    desc = LocalBinaryPatterns(8, 1)
    correct = 0
    num_of_iterarions = 100
    for i in range(num_of_iterarions):
        print("############################iteration number ", i + 1)
        # lists[0]->imagemodels of training
        # lists[1]->labels of training
        # lists[2]->imagemodel of test
        # lists[3]->label of test
        mainlist = dumm.generateTrainingAndTestList()
        while (len(mainlist) == 0):
            mainlist = dumm.generateTrainingAndTestList()
        features = []
        featuresOfTestedImage = []
        labelList = []
        #histograms=[]
        for idx, paper in enumerate(mainlist[0]):
            hist = desc.describe(paper.image_cropped)
            # plt.bar(numpy.arange(0,26),hist)
            # plt.title('writer '+mainlist[1][idx].__str__())
            # plt.show()
            #paper.show(0.4,paper.CROPPED)
            #histograms.append(hist)
            features.append(hist)
            labelList.append(mainlist[1][idx])

        hist = desc.describe(mainlist[2][0].image_cropped)
        # plt.bar(numpy.arange(0, 26), hist)
        # plt.title('test writer ' + mainlist[3][0].__str__())
        # plt.show()
        #mainlist[3][0].show(0.4, mainlist[2][0].CROPPED)
        #histograms.append(hist)
        featuresOfTestedImage.append(hist)

        resultSVM = SVM(features, labelList, numpy.array(featuresOfTestedImage), False)
        ans = resultSVM
        print("SVM say ",ans)
        if ans == mainlist[3][0]:
            correct = correct + 1
            print("succeed the answer is ", mainlist[3][0])
        else:
            print("failed the correct answer is ", mainlist[3][0])
            # for k in range(len(histograms)):
            #     plt.bar(numpy.arange(0,26),histograms[k])
            #     if k==len(histograms)-1:
            #         plt.title('test writer ' + mainlist[3][0].__str__())
            #     else:
            #         plt.title('writer '+mainlist[1][k].__str__())
            #     plt.show()
        print("num of correct=", correct)
    accuracy = correct / num_of_iterarions
    print(accuracy * 100)
