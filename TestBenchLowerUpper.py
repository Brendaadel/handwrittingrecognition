import dataModule
import lowerUpperContour as ULC
from sklearn.neighbors import KNeighborsClassifier
from statistics import mode
from sklearn import preprocessing
import numpy as np
import statistics
from sklearn.mixture import GaussianMixture
from scipy import spatial
from sklearn import svm
from sklearn.model_selection import StratifiedKFold

if __name__ == '__main__':
    dumm = dataModule.data()
    correct = 0
    num_of_iterarions=100
    for i in range(num_of_iterarions):
        print("############################iteration number ",i+1)
        #lists[0]->imagemodels of training
        #lists[1]->labels of training
        #lists[2]->imagemodel of test
        #lists[3]->label of test
        mainlist = dumm.generateTrainingAndTestList()
        while (len(mainlist)==0):
            mainlist = dumm.generateTrainingAndTestList()
        training_features=[]
        training_labels=[]
        for idx,paper in enumerate(mainlist[0]):
            for line in paper.lines:
                uc = ULC.UpperLowerContour(line, True)
                ul = ULC.UpperLowerContour(line, False)
                training_features.append((uc.getFeatureVector()+ul.getFeatureVector())[0])
                training_labels.append(mainlist[1][idx])
        test_features=[]
        for line in mainlist[2][0].lines:
            uct = ULC.UpperLowerContour(line, True)
            ult = ULC.UpperLowerContour(line, False)
            test_features.append((uct.getFeatureVector()+ult.getFeatureVector())[0])

        allFeatures = np.array(training_features+test_features)
        # normalized_features = training_features+test_features
        normalized_features = preprocessing.scale(allFeatures)
        withoutLastOne=normalized_features[:-len(test_features),:]
        lastOne=normalized_features[-len(test_features):,:]


        knn = KNeighborsClassifier(n_neighbors=5)
        knn.fit(withoutLastOne,training_labels)
        lines_ans=knn.predict(lastOne)
        print("Result KNN:    ",lines_ans,np.argmax(np.bincount(np.array(lines_ans))))

        tree = spatial.KDTree(withoutLastOne)
        ansTree = tree.query(lastOne)
        ansTreeLabeled = [training_labels[x] for x in ansTree[1]]
        print("Result KDTREE: ",ansTreeLabeled,np.argmax(np.bincount(np.array(ansTreeLabeled))))


        clf = svm.SVC(gamma='scale')
        clf.fit(withoutLastOne, training_labels)
        resultSVM = clf.predict(lastOne)
        print("Result SVM:    ", resultSVM,np.argmax(np.bincount(np.array(resultSVM))))

        # skf = StratifiedKFold(n_splits=2)
        # skf.get_n_splits(withoutLastOne, training_labels)
        # gmmResults = gmm.score_samples(lastOne)
        # print("Result KFold: ", gmmResults)


        print("CORRECT: ",mainlist[3][0])

        ans = np.argmax(np.bincount(np.array(resultSVM)))

        if ans == mainlist[3][0]:
            correct = correct + 1
        print("num of correct=", correct)

    accuracy=correct/num_of_iterarions
    print(accuracy*100)




