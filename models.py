
from sklearn.neighbors import KNeighborsClassifier
from sklearn.mixture import GaussianMixture
import operator
import numpy
from sklearn import svm
from scipy import spatial
from sklearn.neural_network import MLPClassifier
def KNN (features,labels,test,lines):
    #return predicted label
    #lines = true means writers are represented by lines and not documents
    classifier = KNeighborsClassifier(n_neighbors=3)
    classifier.fit(features, labels)
    if(lines==False):
        predicted_label= classifier.predict([test])
        return predicted_label[0]
    predicted_labels = classifier.predict(test)
    return predicted_labels

def GMM (trainingSet,numOfComponents,featuresOfTestedImage):
    #return the predicted label
    featuresOfTestedImage = featuresOfTestedImage.reshape(1, -1) # need to be of a specific structure
    gmm1 = GaussianMixture(n_components=numOfComponents)
    writer1Data=trainingSet[:2]
    gmm1.fit(writer1Data)
    score1=gmm1.score(featuresOfTestedImage) #return loglikelihood so we need the exponential to make it between 0 and 1

    gmm2 = GaussianMixture(n_components=numOfComponents)
    writer2Data = trainingSet[2:4]
    gmm2.fit(writer2Data)
    score2 = gmm2.score(featuresOfTestedImage)

    gmm3 = GaussianMixture(n_components=numOfComponents)
    writer3Data = trainingSet[4:]
    gmm3.fit(writer3Data)
    score3 = gmm3.score(featuresOfTestedImage)
    listOfScores=[]
    listOfScores.append(score1)
    listOfScores.append(score2)
    listOfScores.append(score3)
    index, value = max(enumerate(listOfScores), key=operator.itemgetter(1))
    return index


def KDtree (featureVectors,featureVectorTestImage):
    tree = spatial.KDTree(featureVectors)
    ans = tree.query(featureVectorTestImage)
    return ans



def SVM (featureVectors,labels,featuresOfTestedImage,lines):
    # lines = true means writers are represented by lines and not documents
    if(lines==False):
        featuresOfTestedImage = featuresOfTestedImage.reshape(1, -1)  # need to be of a specific structure
    clf = svm.SVC(gamma='scale')
    clf.fit(featureVectors,labels)
    if(lines==False):
        return clf.predict(featuresOfTestedImage)[0]
    else:
        return clf.predict(featuresOfTestedImage)
def NN (featureVectors,labels,featuresOfTestedImage):
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes = (3, 1), random_state = 1)
    clf.fit(featureVectors, labels)
    return clf.predict(featuresOfTestedImage)

