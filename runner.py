import lowerUpperContour as ULC
from sklearn import preprocessing
from fractalfeature import fractalfeature_line
import time
import edgeDirection
import os
import imageModel as img
import numpy
from models import SVM
from collections import Counter
import collections

walk_dir = "data"
extension = ".png"
PRINTED_AREA = True
OUTPUT_FILE_RESULTS = "result.txt"
OUTPUT_FILE_TIME = "time.txt"

if __name__ == '__main__':


    dirsDic = {}

    with open(OUTPUT_FILE_TIME, "w") as timeFile:
        timeFile.write('')
    with open(OUTPUT_FILE_RESULTS, "w") as timeFile:
        timeFile.write('')

    dirs = [d for d in os.listdir(walk_dir) if os.path.isdir(os.path.join(walk_dir, d))]
    dirs = sorted(dirs)

    for dI in dirs:
        dirsDic[int(dI)] = dI

    dirsDic = collections.OrderedDict(sorted(dirsDic.items()))


    for ind,testfoldername in dirsDic.items():

        start = time.time()

        testfolderpath = os.path.join(walk_dir, testfoldername)
        testImageObject = img.ImageModel(os.path.join(testfolderpath, "test"+extension), PRINTED_AREA)
        ImagesObjectsList = []
        Labels = [1, 1, 2, 2, 3, 3]

        for onetwothree in [1, 2, 3]:
            writerfolderpath = os.path.join(testfolderpath, onetwothree.__str__())

            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "1"+extension), PRINTED_AREA))
            ImagesObjectsList.append(img.ImageModel(os.path.join(writerfolderpath, "2"+extension), PRINTED_AREA))

        edge_direction_object = edgeDirection.edgeDirection()

        training_features = []
        training_labels = []

        for idx, paper in enumerate(ImagesObjectsList):

            allHistogram = edge_direction_object.get_histogram_values_lines(paper, len(paper.lines))

            for lidx, line in enumerate(paper.lines):
                # list features of training example
                # each one is 1-D array
                uc = ULC.UpperLowerContour(line, True)
                ul = ULC.UpperLowerContour(line, False)

                # concatenate all features lists to 1-D list
                training_features.append(uc.getFeatureVector() +ul.getFeatureVector()+ allHistogram[lidx].tolist())
                training_labels.append(Labels[idx])

        test_features = []

        allHistogramTest = edge_direction_object.get_histogram_values_lines(testImageObject , len(testImageObject .lines))


        for lidx, line in enumerate(testImageObject.lines):

            # list features of testing example
            # each one is 1-D array
            uct = ULC.UpperLowerContour(line, True)
            ult = ULC.UpperLowerContour(line, False)

            # concatenate all features lists to 1-D list
            test_features.append(uct.getFeatureVector() + ult.getFeatureVector()+ allHistogramTest[lidx].tolist())

        # combine and scale all features
        allFeatures = numpy.array(training_features + test_features)
        normalized_features = preprocessing.scale(allFeatures)

        # split training set
        withoutLastOne = normalized_features[:-len(test_features), :]
        lastOne = normalized_features[-len(test_features):, :]

        resultSVM = SVM(withoutLastOne, training_labels, lastOne, True)
        resultSVM = Counter(resultSVM).most_common(1)[0][0]


        end = time.time()

        timeOfIteration=end-start
        timeOfIteration=round(timeOfIteration,2)

        with open(OUTPUT_FILE_TIME, "a") as timeFile:
            timeFile.write(str(timeOfIteration))
            timeFile.write("\n")

        with open(OUTPUT_FILE_RESULTS, "a") as resultFile:
            resultFile.write(str(resultSVM))
            resultFile.write("\n")


