from slantAngle import slant_angle
import imageModel as Img
import cv2


if __name__ == '__main__':
    slant_angle_object=slant_angle(0.75)
    img1 = Img.ImageModel("4.png")
    lines=img1.lines # each line contains many rows
    for x in lines:
        binary_img=img1.to_binary(x)
        slant_angle_object.findContours(binary_img)

