import cv2
import scipy
import numpy as np
import math


class slant_angle:

  def __init__(self,T):
      self.T=T
      self.contours=0


  def findContours(self,line):
      scipy.misc.imsave('brenda.jpg', line)
      imgDimensionX,imgDimensionY=line.shape
      img = np.zeros([imgDimensionX, imgDimensionY], dtype=np.uint8)
      img.fill(255)
      im2, contours, hierarchy = cv2.findContours(line, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
      self.contours=contours
      counter=0
      new_img = np.zeros([imgDimensionX, imgDimensionY], dtype=np.uint8)
      new_img.fill(255)
      for c in contours:
          # c equal contour of one object
        # for element in c:
        #     # element equal a point in a contour
        #     y=element[0][0]
        #     x=element[0][1]
        #     img[x,y]=0
        #new_img=self.approximate_cruve(c,imgDimensionX,imgDimensionY)
        #name= 'curves'+str(counter)+'.png'

          epsilon=cv2.arcLength(c,True)
          approx=cv2.approxPolyDP(c,0.02*epsilon,True)
          for element in c :
              y=element[0][0]
              x=element[0][1]
              new_img[x,y]=0

      scipy.misc.imsave('resultApprox.jpg', new_img)



  def approximate_cruve(self,objectContour,imgDimensionX,imgDimensionY):
      list=[]
      img = np.zeros([imgDimensionX, imgDimensionY], dtype=np.uint8)
      img.fill(255)
      fi=0
      counter=0
      x0=objectContour[0][0][1]
      y0=objectContour[0][0][0]
      lastX=x0
      lastY=y0
      lastFi=fi
      for element in objectContour:
            counter=counter+1
            if(counter>1):
                xi=element[0][1]
                yi=element[0][0]
                deltaX=(xi-x0)-(lastX-x0)
                deltaY=(yi-y0)-(lastY-y0)
                deltaFi=((xi-x0) * deltaY)-((yi-y0)*deltaX)
                fi=lastFi+deltaFi
                xiSquared=(xi-x0)*(xi-x0)
                yiSquared=(yi-y0)*(yi-y0)
                length=math.sqrt(xiSquared+yiSquared)
                if self.T * length >= abs(fi):
                    lastX=xi
                    lastY=yi
                    lastFi=fi
                else:
                    img[x0,y0]=0
                    img[lastX,lastY]=0
                    list.append([x0,y0])
                    list.append([lastX,lastY])
                    x0=xi
                    y0=yi
                    fi=0
                    lastX = x0
                    lastY = y0
                    lastFi = fi
      return img









    



