# Handwritting Recognition

This project is about handwritting recognition using pattern recognition techniques.

## Getting Started
### Prerequisites
Any machine that contains python interpreter version 3.6

### Needed packages
cloudpickle<br />
cycler<br />
dask<br />
decorator<br />
kiwisolver<br />
matplotlib<br />
networkx<br />
numpy<br />
opencv-contrib-python<br />
pandas<br />
Pillow<br />
pyparsing<br />
python-dateutil<br />
pytz<br />
PyWavelets<br />
scikit-image<br />
scikit-learn<br />
scipy<br />
six<br />
sklearn<br />
toolz<br />
You can install all of them by running the following command:
```
pip install requirements.txt

```
***If images extension isn't PNG you can change it from ``extension`` in runner.py*

### Dataset directories hierarchy 
data/<br />
├── 00<br />
│   ├── 1<br />
│   ├── 2<br />
│   └── 3<br />
├── 01<br />
│   ├── 1<br />
│   ├── 2<br />
│   └── 3<br />
├── 02<br />
│   ├── 1<br />
│   ├── 2<br />
│   └── 3<br />
├── 03<br />
│   ├── 1<br />
│   ├── 2<br />
│   └── 3<br />
.<br />
.<br />
.<br />

## Running the code
Run this in terminal
```
python3 runner.py

```
## Authors

* **Bassem Samir** - *Impelementation* - [bassem samir](https://gitlab.com/BassemHermina)
* **Brenda Adel** - *Impelementation* - [brenda adel](https://gitlab.com/Brendaadel)
* **Bishoy Kamal** - *Impelementation* - [bishoy kamal](https://gitlab.com/bishoykamal)
* **Mina Khalil** - *Impelementation* - [mina khalil](https://gitlab.com/minakhalil)


