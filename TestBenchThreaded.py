from multiprocessing import Pool
import dataModule
import lowerUpperContour as ULC
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
import numpy as np
from scipy import spatial
from sklearn import svm
from fractalfeature import fractalfeature_line
import time
import edgeDirection
import os
import imageModel as img


ITERATIONS = 100
THREADS = 2
IS_IAM_DATASET=True

MINA_ENABLED=True
BRENDA_ENABLED=True
BISHO_ENABLED=False

PRINTED_AREA = False
walk_dir = "datakolia"

def findWriter(args):

    #used for radwan dataset
    if not IS_IAM_DATASET:
        mainlist=[[],[1, 1, 2, 2, 3, 3],[],[1]]

        testfolderpath = os.path.join(walk_dir, args[0])
        mainlist[2]=[img.ImageModel(os.path.join(testfolderpath, "test.jpg"), PRINTED_AREA)]

        for onetwothree in [1, 2, 3]:
            writerfolderpath = os.path.join(testfolderpath, onetwothree.__str__())
            mainlist[0].append(img.ImageModel(os.path.join(writerfolderpath, "1.jpg"), PRINTED_AREA))
            mainlist[0].append(img.ImageModel(os.path.join(writerfolderpath, "2.jpg"), PRINTED_AREA))
    #end radwan dataset



    startImport = time.time()

    brendaCalTime = 0
    meAndBishoCalTime=0

    startBrendaTime = time.time()
    edge_direction_object = edgeDirection.edgeDirection()
    endBrendaTime = time.time()
    brendaCalTime += (endBrendaTime - startBrendaTime)


    print("***loaded to thread: ", args[0])
    #
    # lists[0]->imagemodels of training
    # lists[1]->labels of training
    # lists[2]->imagemodel of test
    # lists[3]->label of test

    # used for iam dataset
    if IS_IAM_DATASET:
        dumm = args[1]
        mainlist = dumm.generateTrainingAndTestList()
        while (len(mainlist) == 0):
            mainlist = dumm.generateTrainingAndTestList()
    # end iam dataset

    training_features = []
    training_labels = []

    startCalculations = time.time()

    for idx, paper in enumerate(mainlist[0]):

        startBrendaTime = time.time()
        if BRENDA_ENABLED:
            allHistogram = edge_direction_object.get_histogram_values_lines(paper, len(paper.lines))
        endBrendaTime = time.time()

        brendaCalTime += (endBrendaTime-startBrendaTime)

        for lidx,line in enumerate(paper.lines):

            startMeAndBisho = time.time()

            fVector=[]
            #list features of training example
            #each one is 1-D array
            if MINA_ENABLED:
                uc = ULC.UpperLowerContour(line, True)
                ul = ULC.UpperLowerContour(line, False)
                fVector = fVector + uc.getFeatureVector() + ul.getFeatureVector()

            if BISHO_ENABLED:
                slopes = fractalfeature_line(line)
                fVector = fVector + slopes

            if BRENDA_ENABLED:
                fVector = fVector + allHistogram[lidx].tolist()

            # concatenate all features lists to 1-D list
            training_features.append(fVector)
            training_labels.append(mainlist[1][idx])

            endMeAndBishoTime=time.time()
            meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

    test_features = []

    startBrendaTime = time.time()

    if BRENDA_ENABLED:
        allHistogramTest = edge_direction_object.get_histogram_values_lines(mainlist[2][0], len(mainlist[2][0].lines))

    endBrendaTime = time.time()
    brendaCalTime += (endBrendaTime - startBrendaTime)

    for lidx,line in enumerate(mainlist[2][0].lines):

        startMeAndBisho = time.time()

        fVectorTest = []
        # list features of testing example
        # each one is 1-D array

        if MINA_ENABLED:
            uct = ULC.UpperLowerContour(line, True)
            ult = ULC.UpperLowerContour(line, False)
            fVectorTest = fVectorTest + uct.getFeatureVector() + ult.getFeatureVector()

        if BISHO_ENABLED:
            slopest = fractalfeature_line(line)
            fVectorTest = fVectorTest + slopest

        if BRENDA_ENABLED:
            fVectorTest = fVectorTest + allHistogramTest[lidx].tolist()


        # concatenate all features lists to 1-D list
        test_features.append(fVectorTest)

        endMeAndBishoTime = time.time()
        meAndBishoCalTime += (endMeAndBishoTime - startMeAndBisho)

    # combine and scale all features
    allFeatures = np.array(training_features + test_features)
    normalized_features = preprocessing.scale(allFeatures)

    # split training set
    withoutLastOne = normalized_features[:-len(test_features), :]
    lastOne = normalized_features[-len(test_features):, :]

    endCalculations = time.time()

    #KNN CLASSIFIER
    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(withoutLastOne, training_labels)
    lines_ans = knn.predict(lastOne)
    print("Result KNN:    ", lines_ans, np.argmax(np.bincount(np.array(lines_ans))))

    # KD-tree CLASSIFIER
    tree = spatial.KDTree(withoutLastOne)
    ansTree = tree.query(lastOne)
    ansTreeLabeled = [training_labels[x] for x in ansTree[1]]
    print("Result KDTREE: ", ansTreeLabeled, np.argmax(np.bincount(np.array(ansTreeLabeled))))

    # SVM CLASSIFIER
    clf = svm.SVC(gamma='scale')
    clf.fit(withoutLastOne, training_labels)
    resultSVM = clf.predict(lastOne)
    print("Result SVM:    ", resultSVM, np.argmax(np.bincount(np.array(resultSVM))))


    print("CORRECT: ", mainlist[3][0])

    #TODO : make a voting strategy between the three classifiers
    ansKD  = 1 if np.argmax(np.bincount(np.array(ansTreeLabeled))) == mainlist[3][0] else 0
    ansKNN = 1 if np.argmax(np.bincount(np.array(lines_ans))) == mainlist[3][0] else 0
    ansSVM = 1 if np.argmax(np.bincount(np.array(resultSVM))) == mainlist[3][0] else 0

    print("SUCCESS" if ansSVM == 1 else "FAIL" )

    print("Calculation Time: (",endCalculations - startCalculations,"s)","[Brenda only]:",brendaCalTime," ","[Me&bisho]:",meAndBishoCalTime," ", (endCalculations - startCalculations)*100/(endCalculations - startImport) , "%  ,Img Time:", (startCalculations - startImport)*100/(endCalculations - startImport),"% ,Total:",endCalculations-startImport,"s")

    print("Finish Test For:",args[0] )
    print("############################")

    return [ansKNN,ansKD,ansSVM]


if __name__ == '__main__':

    startTotalTime = time.time()
    dm = dataModule.data()
    p = Pool(THREADS)


    dirs = [d for d in os.listdir('datakolia') if os.path.isdir(os.path.join('datakolia', d))]
    dirs = sorted(dirs)

    #results is array of lenth dirs
    #each element is array of result [knn,kd,svm] of length 3
    #value of each is 1 if succ , 0 fail
    if IS_IAM_DATASET:
        results = p.map(findWriter,[[i, dm] for i in range(ITERATIONS)])
    else:
        results = p.map(findWriter, [[d] for d in dirs])

    print(results)

    accuracyKnn = np.count_nonzero(np.array(results)[:, 0] == 1) * 100 / len(results)
    accuracyKD  = np.count_nonzero(np.array(results)[:, 1] == 1) * 100 / len(results)
    accuracySVM = np.count_nonzero(np.array(results)[:, 2] == 1) * 100 / len(results)

    endTotalTime = time.time()

    print("Knn: ",accuracyKnn," ,KDTree: ",accuracyKD," ,SVM: ",accuracySVM)
    print("Time: ",(endTotalTime-startTotalTime)/60,"Mins")
