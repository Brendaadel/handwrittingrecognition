import math
import cv2
import numpy as np
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
import pandas as pd
from scipy.signal import find_peaks
from collections import Counter
#print(cv2.__version__)


class ImageModel:

    IMAGE_NORMAL = 0
    HISTOGRAM = 1
    HISTOGRAM_DRVTV = 2
    INVERTED = 3
    IMAGE_BINARY = 4
    HORIZONTAL_LINES = 5
    CROPPED = 6
    HISTOGRAM_CROPPED = 7
    LINES = 8

    def __init__(self, path, haveprintedarea=True):
        self.path = path
        self.image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        if self.image is None :
            self.found=False
            return
        self.found=True
        # these binary images are used for cropping and triming, don't change threshold value for any other reason
        _, self.image_binary = cv2.threshold(self.image, 150, 255, cv2.THRESH_BINARY)
        self.lines = []
        self.horizontal_hist = cv2.reduce(255-self.image_binary, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
        self.horizontal_drvtv_hist = cv2.reduce(np.diff(255-self.image_binary), 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
        self.cutting_lines = []
        if haveprintedarea:
            self.__find_horizontal_lines()
            self.image_cropped = self.image[self.cutting_lines[1]+20:self.cutting_lines[2]-20, 60:len(self.image[0])-60]
        else:
            self.image_cropped = self.image[:]  # [:] to force a copy
        _, self.image_binary_cropped = cv2.threshold(self.image_cropped, 150, 255, cv2.THRESH_BINARY)
        self.horizontal_hist_cropped = cv2.reduce(255-self.image_binary_cropped, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
        # EXTENSIVE TESTING el functions di, el loop el bara , ha3adi 3l files kolha kol mra be wa7da mn dol mawgoda
        # wasala7 kol l errors, b3d kda asta5dm code brenda eni agib l features bta3et l lines el 3mltha di mn kol l lines
        # we a3ml classifier ygrab haygib bl feaures di sa7 wla la2
        self.__find_lines_between_lines()
        self.__divide_lines()
        self.__clean_lines()

    def add_line(self, line):
        self.lines.append(line)

    def show(self, scale_ratio, flag=IMAGE_NORMAL):
        cv2.namedWindow('image', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('image', int(len(self.image[0])*scale_ratio), int(len(self.image)*scale_ratio))

        if flag == ImageModel.IMAGE_NORMAL:
            cv2.imshow('image', self.image)
        elif flag == ImageModel.IMAGE_BINARY:
            cv2.imshow('image', self.image_binary)
        elif flag == ImageModel.HISTOGRAM:
            # normalized histogram = histogram array * image width / max value in the histogram array
            normalized_histogram = self.horizontal_hist*len(self.image[0])/np.amax(self.horizontal_hist)
            dummy_image = self.image.copy()
            for i in range(len(normalized_histogram)):
                dummy_image[i, :int(normalized_histogram[i])] = 0
            cv2.imshow('image', dummy_image)
        elif flag == ImageModel.HISTOGRAM_DRVTV:
            normalized_histogram = self.horizontal_drvtv_hist * len(self.image[0]) / np.amax(self.horizontal_drvtv_hist)
            dummy_image = self.image.copy()
            for i in range(len(normalized_histogram)):
                dummy_image[i, :int(normalized_histogram[i])] = 0
            cv2.imshow('image', dummy_image)
        elif flag == ImageModel.INVERTED:
            cv2.imshow('image', 255-self.image)
        elif flag == ImageModel.HORIZONTAL_LINES:
            dummy_image = self.image.copy()
            for i in self.cutting_lines:
                cv2.line(dummy_image, (0, i), (len(dummy_image), i), (0, 0, 255), 3)
            cv2.imshow('image', dummy_image)
        elif flag == ImageModel.CROPPED:
            cv2.resizeWindow('image', int(len(self.image_cropped[0]) * scale_ratio), int(len(self.image_cropped) * scale_ratio))
            cv2.imshow('image', self.image_cropped)
        elif flag == ImageModel.HISTOGRAM_CROPPED:
            cv2.resizeWindow('image', int(len(self.image_cropped[0]) * scale_ratio), int(len(self.image_cropped) * scale_ratio))
            normalized_histogram = self.horizontal_hist_cropped*len(self.image_cropped[0])/np.amax(self.horizontal_hist_cropped)
            dummy_image = self.image_cropped.copy()
            for i in range(len(normalized_histogram)):
                dummy_image[i, :int(normalized_histogram[i])] = 0
            cv2.imshow('image', dummy_image)
        elif flag == ImageModel.LINES:
            cv2.destroyAllWindows()
            for i in range(len(self.lines)):
                cv2.namedWindow('image' + str(i+1), cv2.WINDOW_NORMAL)
                cv2.resizeWindow('image' + str(i+1), int(len(self.lines[0][0]) * scale_ratio), int(len(self.lines[0]) * scale_ratio))
                cv2.imshow('image' + str(i+1), self.lines[i])

        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def __find_horizontal_lines(self):
        # scale_ratio = 0.4
        # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('image', int(len(self.image[0]) * scale_ratio), int(len(self.image) * scale_ratio))

        img = self.image.copy()
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
        # canny edge detection required by the hough transform function
        edges = cv2.Canny(img, 50, 150, apertureSize=3)
        lines = cv2.HoughLinesP(edges, 1, math.pi / 2, 2, None, 30, 1)
        blank_image = np.zeros((len(self.image), len(self.image[0]), 1), np.uint8)
        for x in range(0, len(lines)):
            for x1, y1, x2, y2 in lines[x]:
                if x1 != x2:  # only vertical lines
                    # draw the lines found by Hough function onto a new image
                    cv2.line(blank_image, (x1, y1), (x2, y2), (255, 255, 255), 3)
        # get histogram of the newly created image as it only contains the horizontal lines found by hough
        horizontal_hist_lines = cv2.reduce(blank_image, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
        horizontal_hist_lines = np.concatenate(horizontal_hist_lines, axis=0)
        normalized_histogram = horizontal_hist_lines * len(self.image[0]) / np.amax(horizontal_hist_lines)
        # remove indices containing 0 value from histogram as they will move the average value back into favor of 0
        normalized_histogram_for_average = list(normalized_histogram)
        while 0 in normalized_histogram_for_average:
            normalized_histogram_for_average.remove(0)
        # loop through all histogram values and discard values less than the average value (to get the longest lines)
        denominator = 1.0
        while len(self.cutting_lines) < 3:
            denominator += 0.3
            self.cutting_lines = []
            dummy_image = np.zeros((len(self.image), len(self.image[0]), 1), np.uint8)
            close_to_final_list = []
            average = max(normalized_histogram_for_average)/denominator
            for i in range(len(normalized_histogram)):
                if normalized_histogram[i] > average:
                    dummy_image[i, :int(normalized_histogram[i])] = 255
                    close_to_final_list.append(i)

            # scale_ratio = 0.4
            # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
            # cv2.resizeWindow('image', int(len(self.image[0]) * scale_ratio), int(len(self.image) * scale_ratio))
            # cv2.imshow('image', dummy_image)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            # use mean shift instead as "n02-037.png" had a line which caused an error
            meanshift = MeanShift(bandwidth=100).fit(np.reshape(close_to_final_list, (-1 , 1)))
            # their centers are the horizontal lines
            # int(kmeans.cluster_centers_[0][0]) , [1][0] , [2][0]
            self.cutting_lines = sorted(list(np.concatenate( meanshift.cluster_centers_, axis=0 )))
            for i in range(len(self.cutting_lines)):
                self.cutting_lines[i] = int(self.cutting_lines[i])

    def __find_lines_between_lines(self):
        # get local minimum of the smoothed
        # le kol line , get min pixels line within a window fi loop lghayt ma ysbat
        # 3shan agib 7agm l window. hyb2a rob3 aw nos el line height
        # we hgib l height bel mean shift clustering washof min akbar cluster wa5od l mean bta3ha

        # scale_ratio = 0.4
        # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('image', int(len(self.image[0]) * scale_ratio), int(len(self.image) * scale_ratio))
        # cv2.resizeWindow('image', int(len(self.image_cropped[0]) * scale_ratio), int(len(self.image_cropped) * scale_ratio))

        # # ---------- SMOOTH AND GET LOCAL MINS
        normalized_histogram = self.horizontal_hist_cropped * len(self.image_cropped[0]) / np.amax(self.horizontal_hist_cropped)
        dummy_image = self.image_cropped.copy()
        normalized_histogram = np.concatenate(normalized_histogram, axis=0)
        # create dataframe
        df = pd.DataFrame({'col': normalized_histogram})
        # smooth data curve
        normalized_histogram = df.col.rolling(70).median().tolist()

        # for i in range(len(normalized_histogram)):
        #     if not math.isnan(normalized_histogram[i]):
        #         dummy_image[i][0:int(normalized_histogram[i])] = 0
        # cv2.imshow('image', dummy_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        peaks, _ = find_peaks([len(self.image_cropped[0])-x for x in normalized_histogram])
        # ----------- TRANSLATE EVERY DIVIDER LINE TO THE NEATREST MINIMUM VALUE LOCATION
        line_differences = np.diff(peaks)
        line_differences_more_realistic = []
        for i in line_differences:
            if i > 40:
                line_differences_more_realistic.append(i)
        # cluster them
        meanshift = MeanShift(bandwidth=70).fit(np.reshape(line_differences_more_realistic, (-1, 1)))
        # get the most common line height
        line_height = line_differences_more_realistic[list(meanshift.labels_).index(Counter(meanshift.labels_).most_common(1)[0][0])]
        # now for every line in peaks, loop within a window = line_height/2 untill it settles in the min pixels row
        for i in range(len(peaks)):
            window = int(line_height / 4)
            while self.horizontal_hist_cropped[peaks[i]] > min(self.horizontal_hist_cropped[max(peaks[i]-window, 0):min(peaks[i]+window, len(self.horizontal_hist_cropped))]):
                window_list = self.horizontal_hist_cropped[max(peaks[i] - window, 0):min(peaks[i] + window, len(self.horizontal_hist_cropped))]
                peaks[i] = window_list.tolist().index(min(window_list)) + peaks[i] - window

        self.peaks = sorted(list(set(peaks))) # remove duplicates
        new_peaks = []
        for peak in self.peaks:
            if not (peak+1 in self.peaks or peak-1 in self.peaks):
                new_peaks.append(peak)
        self.peaks = new_peaks
        # # show dividing lines
        for i in range(len(self.peaks)):
            dummy_image[peaks[i]:peaks[i]+5] = 0
        # cv2.imshow('image', dummy_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

    def __divide_lines(self):
        last = 0
        for i in range(len(self.peaks)):
            line = self.image_cropped[last: self.peaks[i]-1]
            line_binary = self.image_binary_cropped[last: self.peaks[i] - 1]
            last = self.peaks[i]
            # if min(map(min, line_binary)) == 255:
            # if sum([255-x for x in line_binary[int(len(line_binary)/2)]]) < 0.05 * len(line_binary):
            if max(np.concatenate(cv2.reduce(255-line_binary, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S))) < 0.05 * len(line_binary[0]) * 255:
                continue
            self.lines.append(line)
        # for the case that there is no horizontal line after the last line
        line = self.image_cropped[last: len(self.image_cropped)]
        line_binary = self.image_binary_cropped[last: len(self.image_cropped)]
        if max(np.concatenate(cv2.reduce(255 - line_binary, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S))) > 0.05 * len(line_binary[0]) * 255:
            self.lines.append(line)

    def show_line(self, i):
        cv2.imshow('image', self.lines[i])
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def __clean_lines(self):
        for i in range(len(self.lines)):
            ret, thresh = cv2.threshold(self.lines[i], 150, 255, 0)
            im2, contours, hierarchy = cv2.findContours(255-thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            # cv2.drawContours(self.lines[i], contours, -1, (155, 155, 155), 15)
            miny = 10000
            maxy = 0
            for c in contours:
                if cv2.contourArea(c) < 23: #kant 2 , b2et 23!
                    continue
                # get the bounding rect
                x, y, w, h = cv2.boundingRect(c)
                if y < miny:
                    miny = y
                if y+h > maxy:
                    maxy = y+h
            self.lines[i] = self.lines[i][miny:maxy]

    def to_binary(self, img):
        _, binary = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY)
        return binary
