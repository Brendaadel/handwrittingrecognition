import numpy
import operator
from preprocessing import sobel_filter,thresholding_img,get_connected_component
class edgeDirection:
    def edge_direction (self,binary_img,connected_component_img,window):
        # get histogram for all the document
        histogram = numpy.zeros(window)
        for index, pixel in numpy.ndenumerate(binary_img):  # index return y,x where y is the vertical x is the horizontal and pixel is the value
             x,y=index
             pixel_val=binary_img[x,y]
             if(pixel_val==255.0):
                 histogram=self.check_connected_component_12(connected_component_img,x,y,histogram)
        #histogram=self.normalize_histogram(histogram)
        return histogram
    def check_connected_component_12(self,connected_component_img,x,y,histogram):
        label_value=connected_component_img[x,y]
        testCount=0;
        numrows = len(connected_component_img)  # x ,vertical
        numcols = len(connected_component_img[0])
        if((y+3 < numcols-1) and (connected_component_img[x,y+3]==label_value)):
            histogram[0]=histogram[0]+1
            testCount=testCount+1
        if(y+3 < numcols-1 and x-1 <numrows-1 and connected_component_img[x -1, y+3] == label_value):
            histogram[1] = histogram[1] + 1
            testCount = testCount + 1
        if (y+3 <numcols-1 and x-2 < numrows-1 and connected_component_img[x -2, y+3] == label_value):
            histogram[2] = histogram[2] + 1
            testCount = testCount + 1
        if (y+3 <numcols-1 and x-3 < numrows-1 and connected_component_img[x -3, y+3] == label_value):
            histogram[3] = histogram[3] + 1
            testCount = testCount + 1
        if (y+2 <numcols-1 and x-3 < numrows-1 and connected_component_img[x -3, y+2] == label_value):
            histogram[4] = histogram[4] + 1
            testCount = testCount + 1
        if (y+1 <numcols-1 and x-3 < numrows-1 and connected_component_img[x -3, y+1] == label_value):
            histogram[5] = histogram[5] + 1
            testCount = testCount + 1
        if (x-3 < numrows-1 and connected_component_img[x -3, y] == label_value):
            histogram[6] = histogram[6] + 1
            testCount = testCount + 1
        if (y-1 <numcols-1 and x-3 < numrows-1 and connected_component_img[x -3, y-1] == label_value):
            histogram[7] = histogram[7] + 1
            testCount = testCount + 1
        if (y-2 <numcols-1 and x-3 < numrows-1 and connected_component_img[x -3, y-2] == label_value):
            histogram[8] = histogram[8] + 1
            testCount = testCount + 1
        if (y-3 <numcols-1 and x-3 < numrows-1 and  connected_component_img[x -3, y-3] == label_value):
            histogram[9] = histogram[9] + 1
            testCount = testCount + 1
        if (y-3 <numcols-1 and x-2 < numrows-1 and connected_component_img[x -2, y-3] == label_value):
            histogram[10] = histogram[10] + 1
            testCount = testCount + 1
        if (y-3 < numcols-1 and x-1 < numrows-1 and connected_component_img[x -1, y-3] == label_value):
            histogram[11] = histogram[11] + 1
            testCount = testCount + 1
        #if(testCount>0):
            #print("test count =",testCount)
        return histogram
    def normalize_histogram(self,histogram):
        total=0
        total=sum(histogram)
        for i in range(len(histogram)):
            histogram[i]=histogram[i]/total
            #print(histogram[i])
        index, value = max(enumerate(histogram), key=operator.itemgetter(1))
        # print( "index=",index)
        # print("value=",value)
        return histogram
    def get_histogram_values_lines(self, imageModel,numberOfLinesUsed):
        window_size = 12
        listOfHistograms=[]
        accumlatedHistogram = numpy.zeros(window_size)
        linesCounter=numberOfLinesUsed
        for i in range(linesCounter):
            img = imageModel.lines[i]
            img = img.astype('int32')
            img_with_edge_detection = sobel_filter(img)
            binary_img = thresholding_img(img_with_edge_detection)
            connected_component_img = get_connected_component(binary_img)
            #show_connected_component(connected_component_img)
            histogram = self.edge_direction(binary_img, connected_component_img, window_size)
            listOfHistograms.append(self.normalize_histogram(histogram))
        return listOfHistograms







