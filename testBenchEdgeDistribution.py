import numpy #btet3mel ma3 arkam
import cv2
from preprocessing import sobel_filter,thresholding_img,get_connected_component,show_connected_component
from edgeDirection import edgeDirection
from models import KNN,SVM
import imageModel as Img
import dataModule
from sklearn import preprocessing
from collections import Counter
from scipy import spatial
import datetime
#leh index=0 ?
#
def test1():
    #testing that each writer is represented by 2 points only
    count = 0
    f = open("result.txt", "a")
    f.write("begin \n")
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy=0
    before = datetime.datetime.now()
    for i in range(100):
        test = dataModule.data()

        mainList = test.generateTrainingAndTestList()
        while (len(mainList)==0):
            mainList = test.generateTrainingAndTestList()
        #mainList[0] contains objects of images of training
        features=[]
        #prepare the test
        featuresOfTestedImage=edge_direction_object.get_histogram_values_half_lines(mainList[2][0])
        for oneImage in mainList[0] :
            #features.append(edge_direction_object.get_histogram_values(oneImage))
            features.append(edge_direction_object.get_histogram_values_half_lines(oneImage))

         #now features have all images features

        allFeatures = features
        allFeatures.append(featuresOfTestedImage)
        normalized_features = preprocessing.scale(allFeatures)
        featuresOfTestedImage = []
        featuresOfTestedImage = normalized_features[6]
        featuresArray = normalized_features[:-1]
        result=KNN(featuresArray, mainList[1], featuresOfTestedImage)
        if(result[0]==mainList[3][0]):
            accuracy=accuracy+1
            print("succeeded label equal",result,mainList[3][0])
            f.write("True \n")
        else :
            f.write("False \n")
            print ("failed wrong label is",result)
        count = count + 1
        print(count)
def test2():

    #testing that each writer is represneted by it's number of lines
    count = 0
    with open("result.txt", "a") as myfile:
        myfile.write("begin \n")
    edge_direction_object = edgeDirection()
    window_size = 12
    accuracy = 0
    numberOfLinesUsed = 5

    for iteration in range(500):

        #generate data
        test = dataModule.data()
        mainList = test.generateTrainingAndTestList()
        while (len(mainList) == 0):
            mainList = test.generateTrainingAndTestList()



        features = []
        featuresOfTestedImage=[]
        labelList=[]
        writerCounter=1
        counter = 0
        numberOfLinesUsed = 5

        # prepare the image test
        allHistogram=edge_direction_object.get_histogram_values_lines(mainList[2][0],numberOfLinesUsed)
        for i in range(len(allHistogram)) :
            featuresOfTestedImage.append(allHistogram[i])



        for oneImage in mainList[0]:
            allHistogram=edge_direction_object.get_histogram_values_lines(oneImage,numberOfLinesUsed)
            for i in range(len(allHistogram)):
                features.append(allHistogram[i])
                labelList.append(writerCounter)
            counter=counter+1
            if(counter==2):#increment label counter each 2 image
                writerCounter = writerCounter + 1
                counter=0



        allFeatures=[]
        for i in range(len(features)):
            allFeatures.append(features[i])
        for i in range(len(featuresOfTestedImage)):
            allFeatures.append(featuresOfTestedImage[i])
        normalized_features = preprocessing.scale(allFeatures)
        linesOfTestedImageCount=len(featuresOfTestedImage)
        linesOfTrainingImagesCount=len(features)
        featuresArray=normalized_features[:linesOfTrainingImagesCount]
        featuresOfTestedImage = []
        featuresOfTestedImage = normalized_features[linesOfTrainingImagesCount:]

        #time to enter values on Models

        #resultKNN = KNN(featuresArray, labelList, featuresOfTestedImage,True)
        #resultKNN=Counter(resultKNN).most_common(1)[0][0]

        #resultSVMList=SVM (featuresArray,labelList,featuresOfTestedImage,True)
        #resultSVM=Counter(resultSVMList).most_common(1)[0][0]

        tree = spatial.KDTree(featuresArray)
        ans = tree.query(featuresOfTestedImage)
        resultKD =ans[1]  # label of the nearest image to the tested image
        KDAnswers=[]

        for i in range (len(resultKD)):
            KDAnswers.append(labelList[ans[1][i]])
        resultKD=Counter(KDAnswers).most_common(1)[0][0]

        #voting time
        # if (resultSVM == resultKNN and resultSVM == resultKD):  # all are equal
        #     result = resultSVM
        # elif (resultSVM == resultKNN or resultSVM == resultKD):  # 2 are equal
        #     result = resultSVM
        # elif (resultKNN == resultKD):  # 2 are equal
        #     result = resultKNN
        # else:
        #     result = resultKD  # nothing is equal


        #Decision time
        #result=resultSVM
        result=resultKD
        if (result == mainList[3][0]):
            accuracy = accuracy + 1
            with open("result.txt", "a") as myfile:
                myfile.write("True\n")
            print("succeeded label equal", mainList[3][0])

        else:
            with open("result.txt", "a") as myfile:
                myfile.write("False \n")
            with open("error.txt", "a") as myfile2:
                myfile2.write(" failed wrong label is")
                myfile2.write(str(result))
                myfile2.write("\n")
                for i in range(len(KDAnswers)):
                    myfile2.write(str(KDAnswers[i]))
                myfile2.write("\n")
                myfile2.write("right one is ")
                myfile2.write( str(mainList[3][0]))

            print("failed wrong label is", result,"shouldbe",mainList[3][0])
        count = count + 1

test2()