
import cv2
import numpy as np
import imageModel as Img
from sklearn.metrics import mean_absolute_error


class FeatureDetection:

    DEBUG = 1
    count = 0

    def __init__(self):
        # ideal line image
        self.image = cv2.imread("ideal 34 76.png", cv2.IMREAD_GRAYSCALE)
        # ideal line top and bottom line
        self.top_line = 34
        self.bottom_line = 76
        _, self.image = cv2.threshold(self.image, 140, 255, cv2.THRESH_BINARY)
        # ------
        self.f1,self.f2,self.f3,self.f4,self.f5,self.f6 = 0,0,0,0,0,0
        self.lines = []

    def calculate_features_all_image(self, imgmodel):
        # feature detection for all lines
        self.lines = []
        for line in imgmodel.lines[:5]:
            self.calculate_features(line)
            # self.lines.append([self.f1, self.f2, self.f3, self.f4, self.f5, self.f6])
            self.lines.append([self.f1, self.f2, self.f3])
            print(self.lines[-1])
        return self.lines

    def calculate_features(self, img):
        y1, y2 = self.get_top_bottom_lines(img)
        self.f1 = y1
        self.f2 = y2 - y1
        self.f3 = len(img) - y2
        self.f4 = self.f1 / self.f2
        self.f5 = self.f1 / self.f3
        self.f6 = self.f2 / self.f3

    def get_top_bottom_lines(self, img, debug=False):
        top_line_translated = int(self.top_line * len(img) / len(self.image))
        bottom_line_translated = int(self.bottom_line * len(img) / len(self.image))
        ideal_line_image = cv2.resize(self.image, (len(img[0]), len(img)))
        hist = cv2.reduce(255 - img, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
        min_sq_error, y1, y2 = 1000000000000, 0, 0
        for y1new in range(1, len(ideal_line_image) - 3, 3):
            for y2new in range(y1new + 4, len(ideal_line_image)-1, 4):
                # print((y1new*len(ideal_line_image) + y2new) / pow(len(ideal_line_image), 2))
                ideal_line_img_after_moving_lines = FeatureDetection.move_two_horizontal_lines(ideal_line_image, top_line_translated, y1new, bottom_line_translated, y2new)
                ideal_line_img_after_moving_lines = ideal_line_img_after_moving_lines.astype(np.uint8)
                hist_ideal_now = cv2.reduce(255 - ideal_line_img_after_moving_lines, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
                errorr = mean_absolute_error(hist_ideal_now, hist)
                if errorr < min_sq_error:
                    min_sq_error, y1, y2 = errorr, y1new, y2new
        # DEBUGGING
        if debug:
            image_new = FeatureDetection.move_two_horizontal_lines(img, y1, y1, y2, y2)
            FeatureDetection.show_image(image_new, y1, y2)
            # FeatureDetection.show_hist(image_new,hist)
            ideal_line = FeatureDetection.move_two_horizontal_lines(self.image, self.top_line, y1, self.bottom_line, y2)
            hist_ideal = cv2.reduce(255 - ideal_line, 1, cv2.REDUCE_SUM, dtype=cv2.CV_32S)
            FeatureDetection.show_image(ideal_line, y1, y2)
            # FeatureDetection.show_hist(ideal_line, hist_ideal)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return y1, y2

    def move_two_horizontal_lines(img, y1,y1new, y2, y2new):
        part1 = img[:y1][:]
        part2 = img[y1:y2][:]
        part3 = img[y2:][:]
        part1 = cv2.resize(part1, (len(img[0]), y1new))
        part2 = cv2.resize(part2, (len(img[0]), y2new-y1new))
        part3 = cv2.resize(part3, (len(img[0]), len(img)-y2new))
        new_image = np.array(list(part1) + list(part2) + list(part3))
        return new_image



    def show_image(image, y1=-1, y2=-1):
        FeatureDetection.count+=1
        scale = 700/1200
        cv2.namedWindow("image" + str(FeatureDetection.count), cv2.WINDOW_NORMAL)
        cv2.resizeWindow('image'+str(FeatureDetection.count), int(len(image[0])*scale), int(len(image)*scale))
        if y1 != -1 and y2 != -1:
            cv2.line(image, (0, y1), (len(image[0]), y1), (0, 0, 0), 2)
            cv2.line(image, (0, y2), (len(image[0]), y2), (0, 0, 0), 2)
        cv2.imshow('image'+str(FeatureDetection.count), image)

    def show_hist(img, hist,y1 = -1, y2=-1):
        normalized_hist = hist * len(img[0]) / (np.amax(hist)*4)
        img = img.copy()
        for i in range(len(normalized_hist)):
            img[i][:int(normalized_hist[i])] = 0
        if y1 != -1 and y2 != -1:
            cv2.line(img, (0, y1), (len(img[0]), y1), (0, 0, 0), 2)
            cv2.line(img, (0, y2), (len(img[0]), y2), (0, 0, 0), 2)
        FeatureDetection.show_image(img)

###### into main to fill a folder with line images
# get all lines to images in folder
# path = r'E:\1Gam3a\data'
# onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
# for file in onlyfiles:
    # print(file)
# file = "a02-072.png"
# img1 = Img.ImageModel(join(path, file))
# for i in range(len(img1.lines)):
#     cv2.imwrite("E:\\1Gam3a\\ideal-lines\\" + file + str(i) + ".png", img1.lines[i])


###### into the main to test this class
# img1 = Img.ImageModel("r02-038.png")
# # img1.show(0.4, Img.ImageModel.LINES)
# Ftr = ftr.FeatureDetection()
# Ftr.get_top_bottom_lines(img1.lines[0], True)