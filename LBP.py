from skimage import feature
import numpy as np
import cv2
import matplotlib.pyplot as plt



class LocalBinaryPatterns:
    def __init__(self, numPoints, radius):
        # store the number of points and radius
        self.numPoints = numPoints
        self.radius = radius

    def describe(self, image, eps=1e-7):
        # compute the Local Binary Pattern representation
        # of the image, and then use the LBP representation
        # to build the histogram of patterns
        ret, thresh = cv2.threshold(image, 150, 255, 0)
        im2, contours, hierarchy = cv2.findContours(255 - thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # cv2.drawContours(self.lines[i], contours, -1, (155, 155, 155), 15)
        miny = 10000
        maxy = 0
        minx=10000
        maxx=0
        for c in contours:
            if cv2.contourArea(c) < 23:  # kant 2 , b2et 23!
                continue
            # get the bounding rect
            x, y, w, h = cv2.boundingRect(c)
            if y < miny:
                miny = y
            if y + h > maxy:
                maxy = y + h
            if x<minx:
                minx=x
            if x+w>maxx:
                maxx=x+w
        image = image[miny:maxy,minx:maxx]
        ret, image = cv2.threshold(image, 150, 255, cv2.THRESH_BINARY_INV)

        kernel = np.ones((3, 3), np.uint8)
        image = cv2.erode(image, kernel, iterations=1)
        print("get descreptor")
        lbp = feature.local_binary_pattern(image,self.numPoints,self.radius, method="nri_uniform")
        (hist, _) = np.histogram(lbp.ravel(),bins=np.arange(0, 256),range=(0, 256))
        # print(hist)


        # normalize the histogram
        hist = hist.astype("float")
        hist /= (hist.sum() + eps)

        # return the histogram of Local Binary Patterns
        return hist