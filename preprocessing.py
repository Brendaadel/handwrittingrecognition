import scipy
from scipy import ndimage #eli fiha sobel
import numpy #btet3mel ma3 arkam
import cv2

def sobel_filter(im):
    # apply sobel filter
    dx = ndimage.sobel(im, 0)  # horizontal derivative
    dy = ndimage.sobel(im, 1)  # vertical derivative
    mag = numpy.hypot(dx, dy)  # magnitude this is root gx square plus gy square
    mag *= 255.0 / numpy.max(mag)  # normalize (Q&D)
    #scipy.misc.imsave('sobel.jpg', mag)  # save the image
    return  mag

def thresholding_img(im):
    # convert grayscale to binary
    retval, binary_img = cv2.threshold(im, 128, 255,cv2.THRESH_BINARY)  # retval,mask_img = cv2.threshold(<image>, <threshold value>, <max value>, <the style of thresholding>)
    #scipy.misc.imsave('BinaryImage.jpg', binary_img)  # mask_img is an array
    return binary_img


def get_connected_component(im):
    binary_img = numpy.uint8(im) # do not know why we need to convert it
    ret, labels = cv2.connectedComponents(binary_img) # labels is the image after each connected component took a label , the max number in this array is the number of connected compoennt
    # print (numpy.amax(labels))
    return labels
