import dataModule
import featureDetection as ftr
import models
from scipy import stats
import imageModel
import saveload
import cv2
import numpy as np
from sklearn import preprocessing
from preprocessing import sobel_filter,thresholding_img,get_connected_component,show_connected_component
from models import KNN
from sklearn import preprocessing
from edgeDirection import edgeDirection
from scipy import spatial
# with open("z-accuracy.txt", "w") as myfile:
#     myfile.write("")

# init
if __name__ == '__main__':
    ftrdetector = ftr.FeatureDetection()
    dumm = dataModule.data()

    correct = 0
    for i in range(1000):
        lists = dumm.generateTrainingAndTestList()
        while (len(lists)==0):
            lists = dumm.generateTrainingAndTestList()

        FeatureVectors = []
        Labels = []
        # feature detection for all lines
        for paper in lists[0]:  # lists[0] is the trainingList 1 1 2 2 3 3
            ftrdetector.calculate_features_all_image(paper)
            FeatureVectors.extend(ftrdetector.lines)
            Labels.extend([lists[1][lists[0].index(paper)]]*len(ftrdetector.lines))
            print(Labels)

        # feature detection for test image lines
        ftrdetector.calculate_features_all_image(lists[2][0])
        featurevectortestimage = ftrdetector.lines.copy()

        # normalizing
        allFeatures = FeatureVectors.copy()
        allFeatures.extend(featurevectortestimage)
        # transpose all vectors to get 6 lists , one for every dimension
        allFeatures = list(map(list, zip(*allFeatures)))
        for raw in allFeatures:
            # normalize each dimension
            allFeatures[allFeatures.index(raw)] = [float(i)/max(raw) for i in raw]
        # re transpose
        allFeatures = list(map(list, zip(*allFeatures)))
        featurevectortestimage = allFeatures[len(FeatureVectors):]
        FeatureVectors = allFeatures[:len(FeatureVectors)]

        # tree = spatial.KDTree(FeatureVectors)
        # ans = tree.query(featurevectortestimage)
        ans = models.KNN(FeatureVectors, Labels, featurevectortestimage, True)
        print(ans)
        if stats.mode(ans)[0][0] == lists[3][0]:
            with open("z-accuracy.txt", "a") as myfile:
                myfile.write("Correct!\n")
                print("correct!")
        else:
            with open("z-accuracy.txt", "a") as myfile:
                myfile.write(":(\n")
                print(":(")





# features.append(featurevector)
#          #now features have all images features
#         features.append(featurevectortestimage)
#         allFeatures=np.array(features)
#         normalized_features=preprocessing.scale(allFeatures)
#         featuresOfTestedImage =[]
#         featuresOfTestedImage =normalized_features[6]
#         featuresArray=normalized_features[:-1]
#         result=KNN(featuresArray, mainList[1], featuresOfTestedImage)
#         if(result[0]==mainList[3][0]):
#             accuracy=accuracy+1
#             print("succeeded label equal", result, mainList[3][0])
#
#         else :
#             print ("failed wrong label is", result)